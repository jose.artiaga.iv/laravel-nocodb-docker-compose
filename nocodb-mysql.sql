# create databases
CREATE DATABASE IF NOT EXISTS `nocodb_database`;
CREATE DATABASE IF NOT EXISTS `laravel_docker`;

# create root user and grant rights
# CREATE USER 'root'@'localhost' IDENTIFIED BY 'local';
# GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
# CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
# GRANT ALL PRIVILEGES ON * . * TO 'newuser'@'localhost';

GRANT ALL ON `nocodb_database`.* TO 'sail'@'%';
GRANT ALL ON `laravel_docker`.* TO 'sail'@'%';
